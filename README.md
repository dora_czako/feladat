Czakó Dóra DGSCMW beadandó feladat.

A feladatban egy szobában egy óra látható. 

A szobában a W-A-S-D segítségével lehet mozogni. 

A SPACE és a C billentyűkkel fel és le lehet mozogni.

A + és - billentyűk segítségével a fény erősségét lehet változtatni. 

A kamera mozgatása egérgomb lenyomásával történik.

A Súgó megnyitására és bezárására az F1 billentyű használható. 
